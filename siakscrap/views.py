from django.http import HttpResponse, JsonResponse, HttpResponseForbidden
from django.shortcuts import render
from .scrapper import scrap
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def index(request):
    if request.method == 'POST':
        result = scrap(request.POST['u'],request.POST['p'])
        if(result == False):
            return HttpResponseForbidden
        else:
            ret = {}
            ret['kelas']=result[0]
            ret['nama']=result[1][0]
            ret['npm']=result[2][0]
            return JsonResponse(ret,safe=False)
    else:
        return HttpResponse('')
