# LINK CONSTANT (DON'T CHANGE)
auth_url = 'https://academic.ui.ac.id/main/Authentication/Index'
changerole_url = 'https://academic.ui.ac.id/main/Authentication/ChangeRole'
jadwal_url = 'https://academic.ui.ac.id/main/CoursePlan/CoursePlanViewClass'
summary_url = 'https://academic.ui.ac.id/main/Academic/Summary'

# MAIN Script
import requests
from lxml import html

def scrap(username, password):
    daftar_kelas = []
    s = requests.Session()
    s.post(auth_url, data={'u':username,'p':password})
    s.get(changerole_url)
    if('Mojavi' not in s.cookies.get_dict() or 'siakng_cc' not in s.cookies.get_dict()):
        return False
    summary_html = s.get(summary_url)
    page_tree = html.fromstring(summary_html.content)
    nama_mhs = page_tree.xpath('//td[../th[contains(.,"Nama")]]//text()')
    npm_mhs = page_tree.xpath('//td[../th[contains(.,"NPM")]]//text()')
    jadwal_html = s.get(jadwal_url)
    page_tree = html.fromstring(jadwal_html.content)
    nama_matkul = page_tree.xpath('//tr//td//span//text()')
    matkul = page_tree.xpath('//tr//td/text()')
    number = 1
    count_temp = 0
    count_mode = False
    jadwal_mode = False
    kelas_mode = False
    dosen_mode = False
    count_mode_skip = 0
    count_temp_jadwal = 0
    jadwal_temp = []
    kelas_temp = []
    dosen_temp =[]
    for x in matkul :
        if x != '':
            if count_mode :
                if count_mode_skip ==0:
                    count_mode_skip +=1
                    continue
                
                if '/2020' in x:
                    count_temp +=1
                    continue
                else:
                    count_mode = False
                    count_mode_skip =0
                    jadwal_mode = True
                    
            if(x == str(number)+'.'):
                count_temp = 0
                count_mode = True
                for i in range(len(jadwal_temp)):
                    hari = jadwal_temp[i].split(',')[0]
                    jam_mulai = jadwal_temp[i].split(' ')[1].split('-')[0].split('.')[0]
                    menit_mulai = jadwal_temp[i].split(' ')[1].split('-')[0].split('.')[1]
                    jam_akhir = jadwal_temp[i].split(' ')[1].split('-')[1].split('.')[0]
                    menit_akhir = jadwal_temp[i].split(' ')[1].split('-')[1].split('.')[1]
                    b = [nama_matkul[number-2],hari, jam_mulai, menit_mulai, jam_akhir, menit_akhir ,kelas_temp[i],';'.join(dosen_temp)]
                    daftar_kelas.append(b)
                jadwal_temp = []
                kelas_temp = []
                dosen_temp = []
                number +=1
                continue
            
            if jadwal_mode:
                if count_temp_jadwal <count_temp :
                    jadwal_temp.append(x)
                    count_temp_jadwal +=1
                    continue
                else:
                    jadwal_mode = False
                    kelas_mode = True
                    count_temp_jadwal = 0

            if kelas_mode :
                if count_temp_jadwal < count_temp:
                    kelas_temp.append(x)
                    count_temp_jadwal+=1
                    continue
                else:
                    kelas_mode = False
                    dosen_mode = True
                    count_temp_jadwal = 0

            if dosen_mode:
                dosen_temp.append(x[2:])
                continue

    for i in range(len(jadwal_temp)):
        hari = jadwal_temp[i].split(',')[0]
        jam_mulai = jadwal_temp[i].split(' ')[1].split('-')[0].split('.')[0]
        menit_mulai = jadwal_temp[i].split(' ')[1].split('-')[0].split('.')[1]
        jam_akhir = jadwal_temp[i].split(' ')[1].split('-')[1].split('.')[0]
        menit_akhir = jadwal_temp[i].split(' ')[1].split('-')[1].split('.')[1]
        b = [nama_matkul[number-2],hari, jam_mulai, menit_mulai, jam_akhir, menit_akhir ,kelas_temp[i],';'.join(dosen_temp)]
        daftar_kelas.append(b)
    print(daftar_kelas)
    return daftar_kelas, nama_mhs, npm_mhs
